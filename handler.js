"use strict";

const merge = require("lodash/merge");
const { graphqlLambda } = require("apollo-server-lambda");
const { makeExecutableSchema } = require("graphql-tools");

const rootResolvers = {
  Query: {
    ping: () => "pong"
  }
};

const typeDefs = [
  `
    schema {
      query: Query
    }

    type Query {
      ping: String
    }
  `
];
const resolvers = merge({}, rootResolvers);

const schema = makeExecutableSchema({ typeDefs, resolvers });

module.exports.graphql = graphqlLambda((event, context) => {
  const headers = event.headers,
    functionName = context.functionName;

  return {
    schema,
    context: {
      headers,
      functionName,
      event,
      context
    }
  };
});
