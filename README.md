# SERVERLESS GRAPHQL BASIC SETUP

Most basic setup to create a graphql api on aws lambdas using apollo and the serverless framework

## Tools
- Apollo Server Lambda ([apollo-server-lambda](https://github.com/apollographql/apollo-server/tree/master/packages/apollo-server-lambda))
- Serveless ([serverless](https://serverless.com/))

## How to Deploy
```bash
npm i
serveless deploy
```

## How to invoke it

```bash

curl --request POST \
  --url {lambda-url-and-env}/graphql \
  --header 'content-type: application/json' \
  --data '{"query":"{\n\tping\n}"}'

```